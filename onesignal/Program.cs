using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using OneSignal.RestAPIv3.Client;
using OneSignal.RestAPIv3.Client.Resources;
using OneSignal.RestAPIv3.Client.Resources.Notifications;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace onesignal
{
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateHostBuilder(args).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();
                });

        //public void Push()
        //{
        //    var client = new OneSignalClient("MDVlYzNhYzktMTUyZS00NzMzLWI3MWEtZWRhNjgwOTExN2I0"); // Use your Api Key

        //    var options = new NotificationCreateOptions
        //    {
        //        AppId = new Guid("c37695e3-d263-4c0c-be35-d18597dc3f91"),   // Use your AppId
        //        IncludePlayerIds = new List<string>()
        //        {
        //            "bd897ef2-076f-4fad-9f28-0cbc2ddc0ee7" // Use your playerId
        //        }
        //    };
        //    options.Headings.Add(LanguageCodes.English, "New Notification!");
        //    options.Contents.Add(LanguageCodes.English, "This will push a real notification directly to your device.");

        //    var result = client.Notifications.Create(options);

        //    Console.WriteLine(JsonConvert.SerializeObject(result));
        //    Console.ReadLine();
        //}
    }
}
